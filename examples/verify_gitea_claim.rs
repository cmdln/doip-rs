use doip::claim::Claim;

#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error>> {
    // Prepare the identity claim
    let uri = "https://codeberg.org/yarmo/gitea_proof";
    let fingerprint = "9f0048ac0b23301e1f77e994909f6bd6f80f485d";
    let mut claim = Claim::new(uri, fingerprint);

    // Find matching service providers
    claim.find_match();

    // Verify the claim
    claim.verify().await;

    // Print the result
    println!("Claim: {:?}", claim);
    
    Ok(())
}