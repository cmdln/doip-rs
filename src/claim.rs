use crate::proof_request_protocols::http;
use crate::service_provider::ServiceProvider;
use crate::service_provider::ServiceProviderIndex;

#[derive(Debug, Deserialize)]
pub struct Claim {
    uri: &'static str,
    fingerprint: &'static str,
    state: State,
    n_matched_service_providers: Option<i16>,
    service_provider: Option<ServiceProvider>,
    verification_result: Option<VerificationResult>
}

#[derive(Debug, Deserialize)]
pub struct VerificationResult {
    result: bool,
    proxy_used: Option<String>
}

#[derive(Debug, Deserialize)]
pub enum State {
    Init,
    Matched,
    Verified
}

impl Claim {
    pub fn new(uri: &'static str, fingerprint: &'static str) -> Self {
        // Initialize a new Claim
        Claim {
            uri: uri,
            fingerprint: fingerprint,
            state: State::Init,
            n_matched_service_providers: None,
            service_provider: None,
            verification_result: None
        }
    }
    pub fn uri(&self) -> &str {
        &self.uri
    }
    pub fn fingerprint(&self) -> &str {
        &self.fingerprint
    }
    pub fn find_match(&mut self) {
        // Initialize to zero the number of matched service providers
        self.n_matched_service_providers = Some(0);

        // Load the index of known service providers
        let service_provider_index = ServiceProviderIndex::load().unwrap();

        // For each known service provider
        for item in service_provider_index.service_providers {
            // If a final service provider was already found, skip this one
            if self.service_provider.is_some() {
                continue;
            }

            // Load the service provider
            let service_provider = ServiceProvider::load(&item.name).unwrap();

            // Test the claim's URI against the service provider and see if they are a match
            let service_provider_match = service_provider.test(self);

            // If a match, increase the number of matched service providers by one
            if service_provider_match {
                self.n_matched_service_providers = Some(self.n_matched_service_providers.unwrap()+1);
            }
            
            // If the matched service provider is unambiguous, set it as the claim's final service provider
            if service_provider_match && !service_provider.claim.uri_is_ambiguous {
                self.n_matched_service_providers = Some(1);
                self.service_provider = Some(service_provider);
            }
        }

        // Set the state to matched
        self.state = State::Matched;
    }
    pub async fn verify(&mut self) {
        // Reject verification if the claim was not matched yet
        if self.n_matched_service_providers == None {
            println!("Claim was not matched yet!");
            return;
        }

        // Load the index of known service providers
        let service_provider_index = ServiceProviderIndex::load().unwrap();

        // For each known service provider
        for item in service_provider_index.service_providers {
            // If the verification was already done, skip this one
            if self.verification_result.is_some() {
                continue;
            }

            // Load the service provider
            let service_provider = ServiceProvider::load(&item.name).unwrap();

            // Test the claim's URI against the service provider and see if they are a match
            let service_provider_match = service_provider.test(self);

            // Skip the service provider if they don't match to the claim's URI
            if !service_provider_match {
                continue;
            }

            // Perform the actual verification
            let is_verified_by_service_provider = http::verify_claim(self, &service_provider).await;

            // Skip the service provider if the verification process was unsuccessful
            if is_verified_by_service_provider.is_none() {
                continue;
            }

            // If the service provide verified the claim, update the claim's data accordingly
            if is_verified_by_service_provider.unwrap() {
                self.verification_result = Some(VerificationResult {
                    result: true,
                    proxy_used: None
                });
                self.service_provider = Some(service_provider);
            }
        }

        // If no service provider verified the claim, update claim's data accordingly
        if self.verification_result.is_none() {
            self.verification_result = Some(VerificationResult {
                result: false,
                proxy_used: None
            })
        }

        // Set the state to verified
        self.state = State::Verified;
    }
}