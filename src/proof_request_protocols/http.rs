use serde_json::{Result, Value};
use crate::claim::Claim;
use crate::service_provider::ServiceProvider;

pub async fn verify_claim(claim: &mut Claim, service_provider: &ServiceProvider) -> Option<bool> {
    // Prepare the proof request URI by replacing its regex placeholders
    let proof_request_uri = service_provider.replace_regex(&claim, &service_provider.proof.request.uri);

    match service_provider.proof.response.format.as_str() {
        "json" => {
            // Fetch the JSON document
            let response = fetch_json(proof_request_uri).await;
    
            if response.is_err() {
                None
            } else {
                let response = response.unwrap();
                // Return whether the target data was found in the proof
                Some(find_target(&response, &service_provider, claim.fingerprint()))
            }
        }
        "plaintext" => {
            // Fetch the plaintext document
            let response = fetch_plainttext(proof_request_uri).await;

            // Generate a correctly-formatted target (usually the fingerprint)
            let formatted_target = generate_target(&service_provider, claim.fingerprint()).unwrap().to_lowercase();

            // Return whether the target data was found in the proof
            if response.unwrap().to_lowercase().contains(&formatted_target) {
                Some(true)
            } else {
                Some(false)
            }
        }
        _ => {
            None
        }
    }
}

// Helper functions
async fn fetch_json(url: String) -> Result<Value> {
    let client = reqwest::Client::new();
    let response = client
        .get(url)
        .header(reqwest::header::CONTENT_TYPE, "application/json")
        .header(reqwest::header::ACCEPT, "application/json")
        .header(reqwest::header::USER_AGENT, format!("doiprs/{}", env!("CARGO_PKG_VERSION")))
        .send()
        .await
        .unwrap()
        .text()
        .await;
    
    let response_json: Value = serde_json::from_str(&response.unwrap())?;
    Ok(response_json)
}
async fn fetch_plainttext(url: String) -> Result<String> {
    let client = reqwest::Client::new();
    let response = client
        .get(url)
        .header(reqwest::header::USER_AGENT, format!("doiprs/{}", env!("CARGO_PKG_VERSION")))
        .send()
        .await
        .unwrap()
        .text()
        .await;
    
    Ok(response.unwrap())
}
fn find_target(object: &Value, service_provider: &ServiceProvider, target: &str) -> bool {
    // Delegate the proof finding process to a different function
    find_target_internal(object, &service_provider, target, 0)
}
fn find_target_internal(object: &Value, service_provider: &ServiceProvider, target: &str, instruction: usize) -> bool {
    if object.is_array() { // If the JSON data is an array
        let mut res = false;

        // Recursively explore each element of the JSON array
        for item in object.as_array().unwrap() {
            if res == true {
                continue;
            }
            res = find_target_internal(&item, &service_provider, target, instruction);
        }
        res
    } else if object.is_object() { // If the JSON data is an object
        // Traverse the JSON object
        let current_instruction = &service_provider.proof.target.as_ref().unwrap().path[instruction];
        find_target_internal(&object[current_instruction], &service_provider, target, instruction+1)
    } else if object.is_string() { // If the JSON data is a string
        // Get the JSON data as string
        let candidate = object.as_str().unwrap();

        // Generate a correctly-formatted target (usually the fingerprint)
        let formatted_target = generate_target(&service_provider, &target).unwrap().to_lowercase();

        // See if the string contains the target
        candidate.to_lowercase().contains(&formatted_target)
    } else {
        false
    }
}
fn generate_target(service_provider: &ServiceProvider, target: &str) -> Option<String> {
    match service_provider.proof.target.as_ref().unwrap().format.as_str() {
        "fingerprint" => Some(format!("{}", target)),
        "uri" => Some(format!("openpgp4fpr:{}", target)),
        "message" => Some(format!("[verifying my openpgp key: openpgp4fpr:{}]", target)),
        _ => None
    }
}