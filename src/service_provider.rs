use config::{ConfigError, Config, File};
use regex::Regex;
use crate::claim::Claim;

#[derive(Debug, Deserialize)]
pub struct ServiceProviderIndex {
    pub service_providers: Vec<ServiceProviderIndexItem>
}
#[derive(Debug, Deserialize)]
pub struct ServiceProviderIndexItem {
    pub name: String
}
#[derive(Debug, Deserialize)]
pub struct ServiceProvider {
    pub about: SPAbout,
    pub profile: SPProfile,
    pub claim: SPClaim,
    pub proof: SPProof
}
#[derive(Debug, Deserialize)]
pub struct SPAbout {
    pub name: String,
    pub shortname: String,
    pub homepage: Option<String>
}
#[derive(Debug, Deserialize)]
pub struct SPProfile {
    pub display: Option<String>,
    pub uri: Option<String>,
    pub qr: Option<String>,
}
#[derive(Debug, Deserialize)]
pub struct SPClaim {
    pub uri_is_ambiguous: bool,
    pub uri: String
}
#[derive(Debug, Deserialize)]
pub struct SPProof {
    pub request: SPProofRequest,
    pub response: SPProofResponse,
    pub target: Option<SPProofTarget>
}
#[derive(Debug, Deserialize)]
pub struct SPProofRequest {
    pub uri: String,
    pub protocol: String,
    pub access_restrictions: Option<String>
}
#[derive(Debug, Deserialize)]
pub struct SPProofResponse {
    pub format: String
}
#[derive(Debug, Deserialize)]
pub struct SPProofTarget {
    pub format: String,
    pub relation: String,
    pub path: Vec<String>
}

impl ServiceProvider {
    pub fn load(name: &str) -> Result<Self, ConfigError> {
        let mut s = Config::default();
        s.merge(File::with_name(&format!("src/config/serviceProviderDefinitions/{}.toml", name)))?;
        s.try_into()
    }
    pub fn test(&self, claim: &Claim) -> bool {
        // Use regular expression to see if a claim's URI matches the service provider
        Regex::new(&self.claim.uri).unwrap().is_match(claim.uri())
    }
    pub fn replace_regex(&self, claim: &Claim, string: &String) -> String {
        // Create a mutable copy of the string to be edited
        let mut output = string.clone();

        // Prepare the regex object for the claim's URI
        let re = Regex::new(&self.claim.uri).unwrap();
        
        // For each regex found, replace it in the string
        for cap in re.captures_iter(claim.uri()) {
            for (i, subcap) in cap.iter().enumerate() {
                if i == 0 {
                    continue;
                }
                if subcap == None {
                    continue;
                }
                let placeholder = format!("{{claim_uri_{}}}", i);
                output = output.replace(&placeholder, subcap.unwrap().as_str());
            }
        }
        output
    }
}

impl ServiceProviderIndex {
    pub fn load() -> Result<Self, ConfigError> {
        let mut s = Config::default();
        s.merge(File::with_name("src/config/serviceProviderDefinitions/index.toml"))?;
        s.try_into()
    }
}