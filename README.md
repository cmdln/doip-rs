# doip-rs

A Rust library to interact with decentralized OpenPGP identity claims and proofs.

## Usage

A quick example to verify a Gitea account:

```rust
use doip::claim::Claim;
use doip::service_provider::ServiceProvider;

// Prepare the identity claim
let uri = "https://codeberg.org/yarmo/gitea_proof";
let fingerprint = "9f0048ac0b23301e1f77e994909f6bd6f80f485d";
let mut claim = Claim::new(uri, fingerprint);

// Find matching service providers
claim.find_match();

// Verify the claim
claim.verify().await;

// Print the result
println!("Claim: {:?}", claim);
```

## About

A `Claim` is an identity claim that needs to be verified. An identity claim is verified when the account it links to and claims possesses the fingerprint of the cryptographic key in which the identity claim is stored (bidirectional linking).

`doip` attempts to match the claim any of the library of service providers based on the URI of the claim. For each service provider match, `doip` makes an HTTP request to the public API associated with the service provider and attemps to find the fingerprint in the data returned by the public API.

If the fingerprint is found, the claim is considered verified.

## Roadmap

- Improve code quality
- Improve documentation
- Add parallel processing of claims
- Add processing of cryptographic keys
- Add processing of signature profiles
- Add additional proof request protocols (DNS, IRC, XMPP…)
- Publish to crates.io

## Contributing

Please report bugs and make feature requests on the [Codeberg issue tracker](https://codeberg.org/keyoxide/doip-rs/issues).

All PRs are welcome.

## License

doip-rs is licensed under [Apache License, Version 2.0](https://codeberg.org/keyoxide/doip-rs/src/branch/main/LICENSE).