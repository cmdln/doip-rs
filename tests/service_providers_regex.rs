use doip::claim::Claim;
use doip::service_provider::ServiceProvider;

#[test]
fn it_accepts_devto_matching_claims() {
    let uri = "https://dev.to/alice/post";
    let fingerprint = "1234567890123456789012345678901234567890";
    let claim = Claim::new(uri, fingerprint);

    let sp = ServiceProvider::load("devto").unwrap();
    assert_eq!(sp.test(&claim), true);
}

#[test]
fn it_rejects_devto_nonmatching_claims() {
    let uri = "https://domain.org/alice/post";
    let fingerprint = "1234567890123456789012345678901234567890";
    let claim = Claim::new(uri, fingerprint);

    let sp = ServiceProvider::load("devto").unwrap();
    assert_eq!(sp.test(&claim), false);
}

#[test]
fn it_accepts_discourse_matching_claims() {
    let uri = "https://domain.org/u/alice";
    let fingerprint = "1234567890123456789012345678901234567890";
    let claim = Claim::new(uri, fingerprint);

    let sp = ServiceProvider::load("discourse").unwrap();
    assert_eq!(sp.test(&claim), true);
}

#[test]
fn it_rejects_discourse_nonmatching_claims() {
    let uri = "https://domain.org/alice";
    let fingerprint = "1234567890123456789012345678901234567890";
    let claim = Claim::new(uri, fingerprint);

    let sp = ServiceProvider::load("discourse").unwrap();
    assert_eq!(sp.test(&claim), false);
}

#[test]
fn it_accepts_gitea_matching_claims() {
    let uri = "https://domain.org/alice/gitea_proof";
    let fingerprint = "1234567890123456789012345678901234567890";
    let claim = Claim::new(uri, fingerprint);

    let sp = ServiceProvider::load("gitea").unwrap();
    assert_eq!(sp.test(&claim), true);
}

#[test]
fn it_rejects_gitea_nonmatching_claims() {
    let uri = "https://domain.org/alice/other_proof";
    let fingerprint = "1234567890123456789012345678901234567890";
    let claim = Claim::new(uri, fingerprint);

    let sp = ServiceProvider::load("gitea").unwrap();
    assert_eq!(sp.test(&claim), false);
}

#[test]
fn it_accepts_github_matching_claims() {
    let uri = "https://gist.github.com/Alice/123456789";
    let fingerprint = "1234567890123456789012345678901234567890";
    let claim = Claim::new(uri, fingerprint);

    let sp = ServiceProvider::load("github").unwrap();
    assert_eq!(sp.test(&claim), true);
}

#[test]
fn it_rejects_github_nonmatching_claims() {
    let uri = "https://domain.org/Alice/123456789";
    let fingerprint = "1234567890123456789012345678901234567890";
    let claim = Claim::new(uri, fingerprint);

    let sp = ServiceProvider::load("github").unwrap();
    assert_eq!(sp.test(&claim), false);
}

#[test]
fn it_accepts_hackernews_matching_claims() {
    let uri = "https://news.ycombinator.com/user?id=Alice";
    let fingerprint = "1234567890123456789012345678901234567890";
    let claim = Claim::new(uri, fingerprint);

    let sp = ServiceProvider::load("hackernews").unwrap();
    assert_eq!(sp.test(&claim), true);
}

#[test]
fn it_rejects_hackernews_nonmatching_claims() {
    let uri = "https://domain.org/user?id=Alice";
    let fingerprint = "1234567890123456789012345678901234567890";
    let claim = Claim::new(uri, fingerprint);

    let sp = ServiceProvider::load("hackernews").unwrap();
    assert_eq!(sp.test(&claim), false);
}

#[test]
fn it_accepts_liberapay_matching_claims() {
    let uri = "https://liberapay.com/alice";
    let fingerprint = "1234567890123456789012345678901234567890";
    let claim = Claim::new(uri, fingerprint);

    let sp = ServiceProvider::load("liberapay").unwrap();
    assert_eq!(sp.test(&claim), true);
}

#[test]
fn it_rejects_liberapay_nonmatching_claims() {
    let uri = "https://domain.org/alice";
    let fingerprint = "1234567890123456789012345678901234567890";
    let claim = Claim::new(uri, fingerprint);

    let sp = ServiceProvider::load("liberapay").unwrap();
    assert_eq!(sp.test(&claim), false);
}

#[test]
fn it_accepts_lichess_matching_claims() {
    let uri = "https://lichess.org/@/Alice";
    let fingerprint = "1234567890123456789012345678901234567890";
    let claim = Claim::new(uri, fingerprint);

    let sp = ServiceProvider::load("lichess").unwrap();
    assert_eq!(sp.test(&claim), true);
}

#[test]
fn it_rejects_lichess_nonmatching_claims() {
    let uri = "https://domain.org/@/Alice";
    let fingerprint = "1234567890123456789012345678901234567890";
    let claim = Claim::new(uri, fingerprint);

    let sp = ServiceProvider::load("lichess").unwrap();
    assert_eq!(sp.test(&claim), false);
}

#[test]
fn it_accepts_lobsters_matching_claims() {
    let uri = "https://lobste.rs/u/Alice";
    let fingerprint = "1234567890123456789012345678901234567890";
    let claim = Claim::new(uri, fingerprint);

    let sp = ServiceProvider::load("lobsters").unwrap();
    assert_eq!(sp.test(&claim), true);
}

#[test]
fn it_rejects_lobsters_nonmatching_claims() {
    let uri = "https://domain.org/u/Alice";
    let fingerprint = "1234567890123456789012345678901234567890";
    let claim = Claim::new(uri, fingerprint);

    let sp = ServiceProvider::load("lobsters").unwrap();
    assert_eq!(sp.test(&claim), false);
}

#[test]
fn it_accepts_mastodon_matching_claims() {
    let uri = "https://domain.org/@alice";
    let fingerprint = "1234567890123456789012345678901234567890";
    let claim = Claim::new(uri, fingerprint);

    let sp = ServiceProvider::load("mastodon").unwrap();
    assert_eq!(sp.test(&claim), true);
}

#[test]
fn it_rejects_mastodon_nonmatching_claims() {
    let uri = "https://domain.org/alice";
    let fingerprint = "1234567890123456789012345678901234567890";
    let claim = Claim::new(uri, fingerprint);

    let sp = ServiceProvider::load("mastodon").unwrap();
    assert_eq!(sp.test(&claim), false);
}

#[test]
fn it_accepts_owncast_matching_claims() {
    let uri = "https://live.domain.org";
    let fingerprint = "1234567890123456789012345678901234567890";
    let claim = Claim::new(uri, fingerprint);

    let sp = ServiceProvider::load("owncast").unwrap();
    assert_eq!(sp.test(&claim), true);
}

#[test]
fn it_accepts_pleroma_matching_claims() {
    let uri = "https://domain.org/users/alice";
    let fingerprint = "1234567890123456789012345678901234567890";
    let claim = Claim::new(uri, fingerprint);

    let sp = ServiceProvider::load("pleroma").unwrap();
    assert_eq!(sp.test(&claim), true);
}

#[test]
fn it_rejects_pleroma_nonmatching_claims() {
    let uri = "https://domain.org/alice";
    let fingerprint = "1234567890123456789012345678901234567890";
    let claim = Claim::new(uri, fingerprint);

    let sp = ServiceProvider::load("pleroma").unwrap();
    assert_eq!(sp.test(&claim), false);
}

#[test]
fn it_accepts_reddit_matching_claims() {
    let uri = "https://www.reddit.com/user/Alice/comments/123456/post";
    let fingerprint = "12345678901234p56789012345678901234567890";
    let claim = Claim::new(uri, fingerprint);

    let sp = ServiceProvider::load("reddit").unwrap();
    assert_eq!(sp.test(&claim), true);
}

#[test]
fn it_rejects_reddit_nonmatching_claims() {
    let uri = "https://domain.org/user/Alice/comments/123456/post";
    let fingerprint = "1234567890123456789012345678901234567890";
    let claim = Claim::new(uri, fingerprint);

    let sp = ServiceProvider::load("reddit").unwrap();
    assert_eq!(sp.test(&claim), false);
}

#[test]
fn it_accepts_twitter_matching_claims() {
    let uri = "https://twitter.com/alice/status/1234567890123456789";
    let fingerprint = "12345678901234p56789012345678901234567890";
    let claim = Claim::new(uri, fingerprint);

    let sp = ServiceProvider::load("twitter").unwrap();
    assert_eq!(sp.test(&claim), true);
}

#[test]
fn it_rejects_twitter_nonmatching_claims() {
    let uri = "https://domain.org/alice/status/1234567890123456789";
    let fingerprint = "1234567890123456789012345678901234567890";
    let claim = Claim::new(uri, fingerprint);

    let sp = ServiceProvider::load("twitter").unwrap();
    assert_eq!(sp.test(&claim), false);
}